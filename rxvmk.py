#~ RXVMK (RX-V Media Keys)
#~ Copyright 2015, Michal Kloko
#~ All Rights Reserved.
#~
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.
#~
#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.
#~
#~ You should have received a copy of the GNU General Public License
#~ along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pyHook, pythoncom, ConfigParser, subprocess, rxv, atexit

VolumeMute = '<Volume><Mute>{value}</Mute></Volume>'

# Key modifier IDs
lshift = 160
lctl = 162
lalt = 164
ralt = 165
rctl = 163
rshift = 161
keymod_ids = [lshift, rshift, lctl, rctl, lalt, ralt]


class RXV(rxv.RXV):
    """
    Extends the RXV library by wuub on github for use with my RXVMK
    class, but separated as it is useful code by itself.
    """
    def __init__(self, AVR_ADDR, AVR_MODEL):
        super(RXV, self).__init__(AVR_ADDR, AVR_MODEL)
        
        self.AVR_MODEL = AVR_MODEL
        self.AVR_ADDR = AVR_ADDR

    @property
    def mute(self):
        """Returns receiver's mute state"""
        request = VolumeMute.format(value = 'GetParam')
        response = self._request('GET', request)
        muted = response.find('Main_Zone/Volume/Mute').text

        if muted == "On": return True
        else: return False

    @mute.setter
    def mute(self, value):
        """Sets reciever's mute state, e.g. self.mute = False"""
        if value == True: value = "On"
        else: value = "Off"

        self._request('PUT', VolumeMute.format(value = value))

    def mute_toggle(self):
        """Toggle receiver's mute state"""
        self.mute ^= True

    def power_toggle(self):
        """Toggle receiver's standby state'"""
        self.on ^= True

    def volup(self, val = 0.5):
        """Increase volume by a passed value. Default and minimum is
        0.5"""
        self.volume += val

    def voldn(self, val = 0.5):
        """Decrease volume by a passed value. Default and minimum is
        0.5"""
        self.volume -= val


class RXVMK(RXV):
    """
    RXVMK (RX-V Media Keys)

    Copyright 2015, Michal Kloko
    All Rights Reserved.

    Released under GNU GPLv3.

    Latest version can be found at:
    https://bitbucket.org/experimentfailed/rxv-media-keys

    Brief: this script allows remote control of a Yamaha RX-V series
    receiver via the special media keys found on many PC keyboards.

    Please review the included readme file for information on usage,
    compatibility, and requirements.

    To run this script in the background, please launch using
    pythonw.exe.
    """
    version = "0.0.10" # Release.Beta.Alpha (_ = extreme changes)

    cfg = ConfigParser.ConfigParser()
    cfg.read('config.ini')

    if cfg.get('GENERAL', 'VERBOSE') == "True": VERBOSE = True
    else: VERBOSE = False
    AVR_MODEL = cfg.get('RECEIVER', 'AVR_MODEL')
    AVR_IP = cfg.get('RECEIVER', 'AVR_IP')
    AVR_ADDR = "http://%s:80/YamahaRemoteControl/ctrl" % AVR_IP
    NIRCMD_PATH = cfg.get('GENERAL', 'NIRCMD_PATH')
    if cfg.get('RECEIVER', 'AUTO_PWR') == "True": AUTO_PWR = True
    else: AUTO_PWR = False
    VOL_UP = int(cfg.get('KEYS', 'VOL_UP'))
    VOL_DN = int(cfg.get('KEYS', 'VOL_DN'))
    MUTE = int(cfg.get('KEYS', 'MUTE'))

    def __init__(self, debug = False):
        self.debug = debug # Print some extra info

        print self.__doc__

        super(RXVMK, self).__init__(self.AVR_ADDR, self.AVR_MODEL)

        self.keyrepeats = 0 # Count key-repeats (held keys)

        self.keysdown = [] # Keys currently being pressed

        # Function names each key should call
        self.keys = {
            self.VOL_UP: self.volup,
            self.VOL_DN: self.voldn,
            self.MUTE: self.mute_toggle
        }

        if not self.on and self.AUTO_PWR: self.on = True # Auto on

        self.keyHook = pyHook.HookManager()
        self.keyHook.KeyDown = self.keyDown
        self.keyHook.KeyUp = self.keyUp
        self.keyHook.HookKeyboard()

        if self.debug or self.VERBOSE:
            print "Valid inputs:\n", self.inputs()

    def _volscale(self, first = 5, step = 8, shiftmod = 2):
        """
        Since when holding a key, we're already sending data to the
        receiver faster than AJAX can go, this function lets us have a
        small acceleration curve by rasing the volume in increasingly
        greater steps, every X amount of keyrepeats.

        e.g. first=5, step = 8 (defaults; can be passed)
        Means for the first 5 keyrepeats, incrase volume by 0.5 for each
        keyrepeat. Then after 8 more repeats, increase volume by 1.0 for
        each keyrepeat, etc up to a value increase of 10.0 at top speed.

        Passing a shiftmod > 0 will make it go even faster when lshift
        is down (does not work well on all keyboards).
        """
        assert shiftmod > 0, "Value less than zero passed for shiftmod"

        val = 0
        if lshift in self.keysdown: val = shiftmod

        if self.keyrepeats <= first: return 0.5 + val
        elif self.keyrepeats <= (first+step): return 1.0 + val
        elif self.keyrepeats <= (first+step*2): return 5.0 + val
        else: return 10.0

    def volup(self):
        """Key-down event for Volume Up key"""
        # Unfortunately, lctl not working with K400r
        if lctl in self.keysdown and not self.keyrepeats:
            if self.input == "TUNER": self.input = "HDMI1"
            else: self.input = "TUNER"
        else: self.volume += self._volscale()

    def voldn(self):
        """Key-down event for Volume Down key"""
        # Unfortunately, lctl not working with K400r
        if lctl in self.keysdown and not self.keyrepeats:
            if self.input == "AV1": self.input = "HDMI1"
            else: self.input = "AV1"
        else: self.volume -= self._volscale()

    def mute_toggle(self):
        """Key-down event for mute key"""
        if lshift in self.keysdown and not self.keyrepeats:
            self.volume -= 20
        else: self.mute ^= True

    def keyDown(self, event):
        """Key-down event for pyHook (self.keyHook)"""
        key = event.KeyID

        if key not in self.keysdown: self.keysdown.append(key)

        # Temporary shut down test
        if(
            lctl in self.keysdown
            and lalt in self.keysdown
            and ralt in self.keysdown
        ): quit()

        if key not in keymod_ids:
            try:
                self.keys[key]()
                self.keyrepeats += 1
                try:
                    subprocess.call(
                        self.NIRCMD_PATH+"nircmd.exe setsysvolume 65535"
                    )
                except:
                    if self.debug: print "Warning: NirCmd not found!"
            except:
                if self.debug: print "Key function failed/not assigned"

        if self.VERBOSE or self.debug: print key

        return True

    def keyUp(self, event):
        """Key-up event for pyHook (self.keyHook)"""
        if event.KeyID not in keymod_ids: self.keyrepeats = 0

        self.keysdown.remove(event.KeyID)

        return True

    def shutdown(self):
        """Puts the receiver in standby and exits the script"""
        if self.AUTO_PWR and not self.debug: self.on = False
        else: print "Shutdown called"

    def start_pump(self):
        """Starts the pythoncom pump"""
        print "RXVMK is now running..."
        pythoncom.PumpMessages()


if __name__ == "__main__":
    rxvmk = RXVMK(debug = 0)
    atexit.register(rxvmk.shutdown)
    rxvmk.start_pump()
