### Abandoned
I no longer have a need for this due to switching my ecosystem to Linux. I will leave this here indefinitely for reference.

### RXVMK (RX-V Media Keys)
Copyright 2015, Michal Kloko
All Rights Reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Known Issues:

- Occasionally goes haywire with my Logitech K400r keyboard, requiring a program restart (have not tested thoroughly with other keyboards yet, but my Logitech G15 seems to work a lot better). It seems to be key-buffer related, and my attempts at controlling it have so far been bust.

- On the same keyboard (K400r), shfit+volume works if you push volume first, then shift a second later; no other modifiers work

- I have been experiencing some crashes as late, but have not had a chance to investigate

### Description:
Basically, this is a semi-hacked together way for me to control my Yamaha RX-V675 with the media keys on my PC keyboard (which is using the receiver as it's primary A/V output). The goal is to reduce or eliminate the need to reach for the IR remote, since my keyboard (Logitech K400r) is usually in my lap already, and I want to control my volume with the receiver - not the PC.

The script sits in the background (without a window, if ran with pythonw.exe), and listens to each key press. When a configured key (media button) is pressed, it uses wuub's RXV library (also not included, but HUGE Thanks to them for releasing it!) to send the command to the receiver. 

NirCmd (not strictly required, and not included, but highly recommended) is used to lock Windows' master volume to 100%, by getting called every time a configured key is pressed. This gives the receiver exclusive control of the volume level. I've tried tried, but so far cannot find any other solution.

### Requirements:
- Windows only, and only tested on W7 Pro. Feel free to port it!

- Python 2.7.9 32-bit (have not tested any other versions)

- pyHook

- pywin32

- RXV library (https://github.com/wuub/rxv or you can just type "pip install rxv" at the command line)

- NirCmd (http://www.nirsoft.net/utils/nircmd.html - I suggest just placing it in the same folder as this script, unless you already have it, then set the path in config.ini)

- A network-connected Yamaha RX-V*73 or RX-V*75 series receiver. Be aware: I've only tested it on an RX-V675. If anyone can test this on other models, and let me know, I would greatly appreciate it, but in theory they should all work!

### Usage:
- Installation:
    1. Put the rxvmk.py and config.ini files anywhere together (same folder)

    2. Edit config.ini, which is mostly self-explanatory. Setting VERBOSE to True will print the corresponding Key ID to the console, whenever you press a key. Use these values, if the default [KEYS] settings do not work.

    3. Optional: of course, you can always create a shortcut in your Start Menu -> All Programs -> Startup folder, to have it run when the desktop loads...

- Key modifiers:

    lshift+volup/dn will increase/decrease the volume at a faster rate(*)

    lshift+mute attenuates the volume (immediately lowers it) by -20db

    Technically, there is currently a lctrl+volup/dn modifier, which will toggle input between HDMI1 and Tuner ctl+volup and HDMI1 and AV1 for ctl+voldn. However, I plan to make some/all of these key modifiers user configurable, so.....more to come!

    (*)As mentioned in the Known Issues above, for keyboards that use Fn+F keys for volume, e.g. Logitech K400r:
    When pressing a modifier+media-key, probably your keyboard is going to send an F key (F7-F12 on the K400r) and not the proper media key. Thus, to use this feature, you may have to press the volume key first, and then press the shift key a half-second, or so, later. I have yet to find a workaround, but will keep trying. And it does go haywire sometimes...

### Auto-Power:
This is not fully implemented, but currently will bring the receiver out of stand-by when the script starts, and will return it to stand-by when the script exits normally. There is also a temporary key-stroke (lctl+lalt+ralt).

You'll also need to enable Network Standby in the receiver's setup menu (consult the receiver's manual, if that option isn't available and the feature doesn't work).

### Closing thoughts:
Hopefully someone out there finds some use for this. I'll be updating it periodically, as I want more functionality for my own use...might as well share!

If you have any feature requests, or find any bugs, let me know and I'll see what I can do. If you'd like to contribute, you'll have to help me, because I am a complete noob to git :)

### Current plans:

- Dig deeper into the AJAX interface to see if we can back-up/restore partial or complete configurations (and in doing so, see what other options we can assign to key-strokes...I'd love a way to toggle "Extra Bass", for instance).

- Pseudo-scriptable key modifiers

- Menu control

- Basically exploit as much of this receiver's AJAX interface as possible until nobody cares any more (which is currently just me >_>)